from __future__ import print_function
import argparse
import os
import random
import torch
import torch.nn as nn
import torchvision.transforms as transforms
import torch.nn.functional as F
import numpy as np
import time
import math
from RT_stereo import RTStereoNet
import cv2
from PIL import Image
import glob

# 2012 data /media/jiaren/ImageNet/data_scene_flow_2012/testing/
#results/exp1/checkpoint_174.tar /home/paperspace/depth/data/inhouse/irset/orig/

parser = argparse.ArgumentParser(description='PSMNet')
parser.add_argument('--KITTI', default='2015',
                    help='KITTI version')
parser.add_argument('--loadmodel', default='rts_100',#checkpoints/exp1
                    help='loading model')
parser.add_argument('--model', default='stackhourglass',
                    help='select model')
parser.add_argument('--maxdisp', type=int, default=192,
                    help='maxium disparity')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

model = RTStereoNet(args.maxdisp)

    #print('no model')

model = nn.DataParallel(model, device_ids=[0])
model.cuda()


args.loadmodel = 'rts_100.tar'
if args.loadmodel is not None:
    print('load PSMNet')
    state_dict = torch.load(args.loadmodel)
    model.load_state_dict(state_dict['state_dict'])

print('Number of model parameters: {}'.format(sum([p.data.nelement() for p in model.parameters()])))

def test(imgL,imgR):
        model.eval()

        if args.cuda:
           imgL = imgL.cuda()
           imgR = imgR.cuda()     

        with torch.no_grad():
            disp = model(imgL,imgR)

        disp = torch.squeeze(disp)
        pred_disp = disp.data.cpu().numpy()

        return pred_disp

import os
def main():

        normal_mean_var = {'mean': [0.485, 0.456, 0.406],
                            'std': [0.229, 0.224, 0.225]}
        infer_transform = transforms.Compose([transforms.ToTensor(),
                                              transforms.Normalize(**normal_mean_var)])
        left_dir = './data/image_2/'

        os.system('mkdir -p out/; rm -rf out/*')
        fnames = glob.glob(left_dir + '/*')
        nimgs = len(fnames)
        total_time = 0
        for lname in fnames:
            rname = lname.replace('image_2','image_3')
            dname = lname.replace('image_2','disp_occ_0')
            d_img = cv2.imread(dname, cv2.IMREAD_UNCHANGED)
            try:
                d_img = cv2.resize(d_img, (512, 256))
            except:
                continue
            mask = d_img<15
            imgL_o = Image.open(lname).convert('RGB')
            imgR_o = Image.open(rname).convert('RGB')
            imgL_o = imgL_o.resize((512, 256))
            imgR_o = imgR_o.resize((512, 256))

            imgL = infer_transform(imgL_o)
            imgL = imgL.unsqueeze(0)

            imgR = infer_transform(imgR_o)
            imgR = imgR.unsqueeze(0)

            start_time = time.time()
            img = test(imgL,imgR)
            total_time += time.time()-start_time

            img = img.astype(np.float32)
            img = img/np.max(img)
            img = img*128*256
            img[img>50000] = 50000
            img = img.astype(np.uint16)
            img[mask] = 0
            #img = img.astype(np.float32)
            #img = np.uint8(255*img/np.max(img))
            if len(d_img.shape)>2:
                d_img = d_img[:,:,0]
            img = np.vstack((d_img, img))
            oname = 'out/' + lname.split('/')[-1]
            cv2.imwrite(oname, img)
            print (oname)
        print('time in ms = %.2f' %((1000*total_time)/nimgs))


if __name__ == '__main__':
   main()






