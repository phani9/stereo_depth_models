import argparse
import os
import glob
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim as optim
import torch.utils.data
import torch.nn.functional as F
import time
import os
import torch
import torch.utils.data as data
import torch
import torchvision.transforms as transforms
import random
from PIL import Image, ImageOps
import numpy as np
import models.preprocess as preprocess
from random import uniform
import cv2
import os


import torch.backends.cudnn as cudnn

import models.anynet
import cv2
import numpy as np

parser = argparse.ArgumentParser(description='Anynet fintune on KITTI')
parser.add_argument('--maxdisp', type=int, default=192,
                    help='maxium disparity')
parser.add_argument('--loss_weights', type=float, nargs='+', default=[0.25, 0.5, 1., 1.])
parser.add_argument('--max_disparity', type=int, default=192)
parser.add_argument('--maxdisplist', type=int, nargs='+', default=[12, 3, 3])
parser.add_argument('--datatype', default='2015',
                    help='datapath')
parser.add_argument('--datapath', default='test_images/', help='datapath')
parser.add_argument('--epochs', type=int, default=3,
                    help='number of epochs to train')
parser.add_argument('--train_bsize', type=int, default=16,
                    help='batch size for training (default: 6)')
parser.add_argument('--test_bsize', type=int, default=8,
                    help='batch size for testing (default: 8)')
parser.add_argument('--save_path', type=str, default='results/finetune_anynet',
                    help='the path of saving checkpoints and log')
#parser.add_argument('--resume', type=str, default='ckpt/anynet.tar',
parser.add_argument('--resume', type=str, default='ckpt/anynet_zed_v1.tar',
                    help='resume path')
parser.add_argument('--lr', type=float, default=5e-5,
                    help='learning rate')
parser.add_argument('--init_channels', type=int, default=1, help='initial channels for 2d feature extractor')
parser.add_argument('--resize', type=int, default=0, help='run the model at 256x256 resolution')
parser.add_argument('--resolution', type=int, default=1, help='initial channels for 2d feature extractor')
parser.add_argument('--nblocks', type=int, default=2, help='number of layers in each stage')
parser.add_argument('--channels_3d', type=int, default=4, help='number of initial channels 3d feature extractor ')
parser.add_argument('--layers_3d', type=int, default=4, help='number of initial layers in 3d network')
parser.add_argument('--growth_rate', type=int, nargs='+', default=[4,1,1], help='growth rate in the 3d network')
parser.add_argument('--spn_init_channels', type=int, default=8, help='initial channels for spnet')
#parser.add_argument('--start_epoch_for_spn', type=int, default=1)


args = parser.parse_args()

def main():
    global args
    args.with_spn=1
    base_dir = args.datapath

    model = models.anynet.AnyNet(args)
    model = nn.DataParallel(model).cuda()
    optimizer = optim.Adam(model.parameters(), lr=args.lr, betas=(0.9, 0.999))

    checkpoint = torch.load(args.resume)
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    cudnn.benchmark = True
    start_full_time = time.time()
    test(model, base_dir)
    return


def test(model, base_dir):
    stages = 4
    size_ = (1232, 368)
    model.eval()
    processed = preprocess.get_transform(augment=False)
    left_dir = base_dir + '/image_2/'
    left_names = glob.glob(left_dir + '/*')
    left_img = Image.open(left_names[0]).convert('RGB')
    left_img = left_img.resize(size_)
    left_img   = processed(left_img)
    right_img = Image.open(left_names[0]).convert('RGB')
    right_img = right_img.resize(size_)
    right_img   = processed(right_img)
    imgL = left_img.float().unsqueeze(0).cuda()
    imgR = right_img.float().unsqueeze(0).cuda()
    max_disp = 192

    #warm up
    for _ in range(10):
            outputs = model(imgL, imgR)

    for i, left_name in enumerate(left_names):
            left_img = Image.open(left_name).convert('RGB')
            left_img = left_img.resize(size_)
            left_img   = processed(left_img)

            right_name = left_name.replace('image_2','image_3')
            right_img = Image.open(right_name).convert('RGB')
            right_img = right_img.resize(size_)
            right_img   = processed(right_img)

            disp_name = left_name.replace('image_2','disp_occ_0')
            dmap = np.ones(size_,dtype=np.uint8)
            try:
                dmap= cv2.imread(disp_name, -1)
                dmap = cv2.resize(dmap, size_)

                dmap = dmap.astype(np.float32)/256
                mask = dmap<5
                mask[dmap>max_disp] = 0
            except:
                continue
            dmap = dmap
            imgL = left_img.float().unsqueeze(0).cuda()
            imgR = right_img.float().unsqueeze(0).cuda()

            with torch.no_grad():
                st = time.time()
                outputs = model(imgL, imgR)
                output = outputs[2]
                output = torch.squeeze(output, 1).cpu()
            img_cpu = np.asarray(output.cpu())[0]
            img_save = 2*np.clip(img_cpu, 0, 255)
            img = img_save.astype(np.float32)
            img[mask] = 0
            img = np.vstack((dmap, img))
            img = cv2.resize(img, (512, 512))
            name = 'out/' + left_name.split('/')[-1]
            cv2.imwrite(name, img)
            print (name)

if __name__ == '__main__':
    main()
