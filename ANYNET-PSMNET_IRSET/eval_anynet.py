import argparse
import os
import glob
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim as optim
import torch.utils.data
import torch.nn.functional as F
import time
import os
import torch
import torch.utils.data as data
import torch
import torchvision.transforms as transforms
import random
from PIL import Image, ImageOps
import numpy as np
import models.preprocess as preprocess
from random import uniform
import cv2


import torch.backends.cudnn as cudnn

import models.anynet
import cv2
import numpy as np

parser = argparse.ArgumentParser(description='Anynet fintune on KITTI')
parser.add_argument('--maxdisp', type=int, default=192,
                    help='maxium disparity')
parser.add_argument('--loss_weights', type=float, nargs='+', default=[0.25, 0.5, 1., 1.])
parser.add_argument('--max_disparity', type=int, default=192)
parser.add_argument('--maxdisplist', type=int, nargs='+', default=[12, 3, 3])
parser.add_argument('--datatype', default='2015',
                    help='datapath')
parser.add_argument('--datapath', default=None, help='datapath')
parser.add_argument('--epochs', type=int, default=3,
                    help='number of epochs to train')
parser.add_argument('--train_bsize', type=int, default=16,
                    help='batch size for training (default: 6)')
parser.add_argument('--test_bsize', type=int, default=8,
                    help='batch size for testing (default: 8)')
parser.add_argument('--save_path', type=str, default='results/finetune_anynet',
                    help='the path of saving checkpoints and log')
parser.add_argument('--resume', type=str, default='ckpt/anynet.tar',
                    help='resume path')
parser.add_argument('--lr', type=float, default=5e-5,
                    help='learning rate')
parser.add_argument('--init_channels', type=int, default=1, help='initial channels for 2d feature extractor')
parser.add_argument('--resize', type=int, default=0, help='run the model at 256x256 resolution')
parser.add_argument('--resolution', type=int, default=1, help='initial channels for 2d feature extractor')
parser.add_argument('--nblocks', type=int, default=2, help='number of layers in each stage')
parser.add_argument('--channels_3d', type=int, default=4, help='number of initial channels 3d feature extractor ')
parser.add_argument('--layers_3d', type=int, default=4, help='number of initial layers in 3d network')
parser.add_argument('--growth_rate', type=int, nargs='+', default=[4,1,1], help='growth rate in the 3d network')
parser.add_argument('--spn_init_channels', type=int, default=8, help='initial channels for spnet')
#parser.add_argument('--start_epoch_for_spn', type=int, default=1)


args = parser.parse_args()

def main():
    global args
    args.with_spn=1

    if not os.path.isdir(args.save_path):
        os.makedirs(args.save_path)

    model = models.anynet.AnyNet(args)
    model = nn.DataParallel(model).cuda()
    optimizer = optim.Adam(model.parameters(), lr=args.lr, betas=(0.9, 0.999))

    checkpoint = torch.load(args.resume)
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    cudnn.benchmark = True
    start_full_time = time.time()
    test(model)


def test(model):
    stages = 4
    model.eval()
    count = 1000
    processed = preprocess.get_transform(augment=False)
    left_names = glob.glob('data/image_2/*')
    left_img = Image.open(left_names[0]).convert('RGB')
    left_img   = processed(left_img)
    right_img = Image.open(left_names[0]).convert('RGB')
    right_img   = processed(right_img)
    imgL = left_img.float().unsqueeze(0).cuda()
    imgR = right_img.float().unsqueeze(0).cuda()

    #warm up
    for i in range(10):
            outputs = model(imgL, imgR)
            print (i)

    total_time = 0
    count = 0
    for i, left_name in enumerate(left_names):
            print (left_name)
            left_img = Image.open(left_name).convert('RGB')
            left_img   = processed(left_img)

            right_name = left_name.replace('image_2','image_3')
            right_img = Image.open(right_name).convert('RGB')
            right_img   = processed(right_img)

            disp_name = left_name.replace('image_2','disp_occ_0')
            dmap= cv2.imread(disp_name, -1)
            dmap = dmap.astype(np.float32)
            dmap = dmap/np.max(dmap)
            imgL = left_img.float().unsqueeze(0).cuda()
            imgR = right_img.float().unsqueeze(0).cuda()

            with torch.no_grad():
                st = time.time()
                outputs = model(imgL, imgR)
                if 1:
                   elapsed_time = time.time()-st
                   total_time += elapsed_time
                   count+=1
                   print (count, elapsed_time )
                output = outputs[2]
                output = torch.squeeze(output, 1).cpu()
                img_cpu = np.asarray(output.cpu())[0]
                img_save = np.clip(img_cpu, 0, 2**16)
                img = img_save.astype(np.float32)
                img = (img/np.max(img))
                img = np.hstack((dmap, img))
                bot = (255*256*img).astype(np.uint16)
                left = cv2.imread(left_name, 0).astype(np.uint16)*256
                right = cv2.imread(right_name, 0).astype(np.uint16)*256
                top = np.hstack((left, right))
                img = np.vstack((top,bot))
                img = cv2.resize(img, (512, 512))
                name = left_name.replace('image_2','anynet_out')
                cv2.imwrite(name, img)
    print ('forward pass time in ms: ', 1000*total_time/(count))
if __name__ == '__main__':
    main()
